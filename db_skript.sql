CREATE TABLE twitter_user (
	id  				serial PRIMARY KEY
,	handle   			text NOT NULL UNIQUE
);

CREATE TABLE hashtag (
  id  			serial PRIMARY KEY
, hash_tag_text     	text NOT NULL
);

CREATE TABLE tweet (
  id			serial PRIMARY KEY
, retweet_count    	int NOT NULL DEFAULT 0
, favorite_count    	int NOT NULL DEFAULT 0
, tweet_text    	text NOT NULL
, tweet_time    	timestamp NOT NULL
, original_author      	text
, CONSTRAINT vorgaenger_ID FOREIGN KEY(id) REFERENCES tweet(id)
);

CREATE TABLE tweet_hat_hashtag (
  FK_tweet integer
, FK_hashtag integer
, hashtag_text  text NOT NULL
, FOREIGN KEY(FK_tweet) REFERENCES tweet(id) ON UPDATE CASCADE ON DELETE CASCADE
, FOREIGN KEY(FK_hashtag) REFERENCES hashtag (id) ON UPDATE CASCADE ON DELETE CASCADE
, PRIMARY KEY (FK_tweet, FK_hashtag)
);

